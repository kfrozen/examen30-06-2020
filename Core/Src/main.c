/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define Orden_filtro 4
//#define Orden_filtroIIR 3

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart2;

SemaphoreHandle_t SemaforoADC;
SemaphoreHandle_t SemaforoFiltro;
QueueHandle_t Cola_Datos;
QueueHandle_t Salida_Datosx;

osThreadId defaultTaskHandle;
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
void StartDefaultTask(void const * argument);
void Toma_Datos (void const * argument);
void Procesa_Datos (void const * argument);
void Salida_Datos(void const * argument);
uint32_t UNSAM_ADC (void);
void UNSAM_DAC(unsigned int);






//void UNSAM_filtroIIR(void const*);
//void UNSAM_SetOut(void const*);


/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  SemaforoADC = xSemaphoreCreateBinary();
  SemaforoFiltro = xSemaphoreCreateBinary();
  xSemaphoreGive(SemaforoADC);
  xSemaphoreTake(SemaforoFiltro,portMAX_DELAY);
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  Cola_Datos = xQueueCreate( Orden_filtro, sizeof(uint16_t) );
  Salida_Datosx = xQueueCreate( Orden_filtro, sizeof(float) );
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  /*osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);*/
  /*defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);*/
xTaskCreate((void *)Toma_Datos, "ADC", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 3, NULL);
xTaskCreate((void *)Salida_Datos, "DAC", configMINIMAL_STACK_SIZE, NULL,  tskIDLE_PRIORITY + 1, NULL);
xTaskCreate((void *)Procesa_Datos, "FIR", configMINIMAL_STACK_SIZE, NULL,  tskIDLE_PRIORITY + 2, NULL);
//xTaskCreate((void *)UNSAM_filtroIIR, "IIR", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);



  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  /*osKernelStart(); */
  vTaskStartScheduler();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */


/* USER CODE END Header_StartDefaultTask */
void Toma_Datos(void const * argumento)
{
	uint16_t dato;
    int i;

	while(1){
		xSemaphoreTake(SemaforoADC,portMAX_DELAY);

		for(i=0;i<Orden_filtro;i++){
		dato=UNSAM_ReadADC();
		xQueueSend( Cola_Datos, &dato, portMAX_DELAY );
		vTaskDelay(100 / portTICK_RATE_MS);
		}
		xSemaphoreGive(SemaforoFiltro);
	}

}

void UNSAM_filtroFIR(void const * argumento)
{
	uint16_t dato[Orden_filtro]={0};

	static int pointer=(Orden_filtro-1);
	static int i;
	static float Y;
	static float x[Orden_filtro]={0};
	static float Ys;
	float b[Orden_filtro]={0.25,0.30,0.35,0.40};
	float c[Orden_filtro]={0.25,0.25,0.25,0.25};
	static float salida;

	while(1){

		xSemaphoreTake(SemaforoFiltro,portMAX_DELAY);

		for(i=0;i<Orden_filtro;i++){
		xQueueReceive( Cola_Datos, &dato[i], portMAX_DELAY );
		}

		Y=0;

		for(i=pointer;i<(pointer+Orden_filtro);i++){
		Y +=  dato[i%Orden_filtro]*b[i-pointer]; /* sopongo que me llagan los datos y los coeficientes para multiblicar directamente */
		}
				pointer=(pointer + 1)%Orden_filtro;

		for(i=0; i<(Orden_filtro-1); i++)
						{
			x[i]=x[i+1];
						}

		x[Orden_filtro-1]=Y;

		for(i=0;i<Orden_filtro;i++)
		{
			Ys+= x[i]*c[i];

		}



		xQueueSend( Salida_Datosx, &Ys, portMAX_DELAY );





		xSemaphoreGive(SemaforoADC);
		}
}
/*void UNSAM_filtroIIR(void const * argumento)
{
	uint16_t dato[Orden_filtroIIR]={0};
	float y[Orden_filtroIIR]={0};
	uint16_t recibido[Orden_filtro];
	static int pointerx=0;
	static int pointery=0;
	static int i;

	float b[Orden_filtroIIR]={0.2,0.3,0};
	float c[Orden_filtroIIR]={0.2,0.3,0.3};
	static float salida[Orden_filtro];

	while(1){

		xSemaphoreTake(SemaforoFiltro,portMAX_DELAY);

		for(i=0;i<Orden_filtro;i++){
		xQueueReceive( QueueADC, &recibido[i], portMAX_DELAY );
		}

		dato[pointerx]=recibido[pointerx];

		for(i=pointery; i<(pointery+Orden_filtroIIR); i++)
		{

		salida[pointery]+=dato[i%(Orden_filtroIIR-1)]*b[(unsigned int)(i-pointery)%(Orden_filtroIIR-1)]-y[i%Orden_filtroIIR]*c[i-pointery];

		}
		pointerx=(pointerx + 1)%(Orden_filtroIIR-1);
		pointery=(pointery + 1)%Orden_filtroIIR;


		y[pointery]=salida[pointery];
		if(pointerx==0){
		for(i=0;i<(Orden_filtroIIR-1);i++){
		xQueueSend( QueueDAC, &salida[i], portMAX_DELAY );
		salida[i]=0;
		}
		}

		xSemaphoreGive(SemaforoADC);
		}
}*/
void Salida_Datos(void const * argumento)
{
	float dato;
	while(1){
		xQueueReceive( Salida_Datosx, &dato, portMAX_DELAY );
		UNSAM_DAC((unsigned int)dato);
		}
}

int UNSAM_ReadADC(void)
{
	return 0;
}
void UNSAM_DAC (unsigned int dato)
{

}

void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN 5 */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END 5 */
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
